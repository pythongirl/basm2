//! Error types for common use

use super::{Interner, Locatable, Location};
use crate::{codegen, lexer, parser};

/// The specific FileId for basm
pub type FileId = ();

/// Common error type for all parts of the assembler
#[derive(derive_more::From, Debug)]
pub enum Error {
    /// Errors that come from the lexer
    #[from]
    Lexical(lexer::LexerError),

    /// Errors that come from inside the parser
    #[from]
    Syntax(parser::SyntaxError),

    /// Errors from codegen
    #[from]
    Semantic(codegen::SemanticError),

    // This could have ! as it's error type because I want that variant to be
    // unconstructable.
    /// Errors that come from `lalrpop` itself
    Parser {
        file: FileId,
        error: lalrpop_util::ParseError<codespan::ByteIndex, lexer::Token, ()>,
    },
}

impl From<(FileId, parser::ParseError)> for Error {
    fn from(parse_error: (FileId, parser::ParseError)) -> Error {
        match parse_error.1 {
            parser::ParseError::User { error } => error,
            error => Error::Parser {
                file: parse_error.0,
                error: error.map_error(|_| ()),
            },
        }
    }
}

/// The specific diagnostic type for basm
pub(crate) type Diagnostic = codespan_reporting::diagnostic::Diagnostic<FileId>;

use codespan_reporting::diagnostic;

pub(crate) mod diagnostic_utils {
    //! Utilities for creating [`Diagnostic`]s
    use super::*;

    fn make_label(l: Location, style: diagnostic::LabelStyle) -> diagnostic::Label<FileId> {
        diagnostic::Label::new(style, l.file_id(), l.span())
    }

    /// Create a primary label from a [`Location`]
    pub(crate) fn make_primary(l: Location) -> diagnostic::Label<FileId> {
        make_label(l, diagnostic::LabelStyle::Primary)
    }

    /// Create a secondary label from a [`Location`]
    pub(crate) fn make_secondary(l: Location) -> diagnostic::Label<FileId> {
        make_label(l, diagnostic::LabelStyle::Secondary)
    }
}

/// A trait for Compiler errors to generate [`Diagnostic`]s
pub(crate) trait CompilerError {
    /// Generate a [`Diagnostic`] with interner context
    fn into_diagnostic(&self, interner: &Interner) -> Diagnostic;
}

use diagnostic_utils::make_primary;

impl CompilerError for Error {
    fn into_diagnostic(&self, interner: &Interner) -> Diagnostic {
        match self {
            Self::Lexical(e) => e.into_diagnostic(interner),
            Self::Syntax(e) => e.into_diagnostic(interner),
            Self::Semantic(e) => e.into_diagnostic(interner),
            Self::Parser { file, error } => {
                use lalrpop_util::ParseError;
                match error {
                    ParseError::InvalidToken { location } => {
                        let location = Location::from_position(*file, *location);

                        Diagnostic::error()
                            .with_message("Invalid Token")
                            .with_labels(vec![make_primary(location)])
                    }
                    ParseError::UnrecognizedEOF { location, expected } => {
                        let location = Location::from_position(*file, *location);

                        let mut note = String::from("Expected one of: ");
                        for tok in expected {
                            note.push_str(&format!("{}, ", tok))
                        }

                        Diagnostic::error()
                            .with_message("Unrecognized end of file")
                            .with_labels(vec![make_primary(location)])
                            .with_notes(vec![note])
                    }
                    ParseError::UnrecognizedToken { token, expected } => {
                        let token: Locatable<lexer::Token> = (*token).into();

                        let mut note = String::from("Expected one of: ");
                        for tok in expected {
                            note.push_str(&format!("{}, ", tok))
                        }

                        Diagnostic::error()
                            .with_message("Unrecognized token")
                            .with_labels(vec![make_primary(token.location)])
                            .with_notes(vec![note])
                    }
                    ParseError::ExtraToken { token } => {
                        let token: Locatable<lexer::Token> = (*token).into();

                        Diagnostic::error()
                            .with_message("Extra token")
                            .with_labels(vec![make_primary(token.location)])
                    }
                    ParseError::User { error: _ } => unreachable!(),
                }
            }
        }
    }
}
