//! Error type for the lexer

use crate::common::{
    error::{CompilerError, Diagnostic},
    Interner, Location,
};

/// Errors from the lexer
#[derive(Debug, Clone)]
pub enum LexerError {
    /// An integer literal could not be parsed
    InvalidIntegerLiteral {
        loc: Location,
        error: lexical_core::Error,
    },
    /// An invalid token was found in the input
    InvalidToken { loc: Location },
    /// A string literal had an invalid escape character
    StringInvalidEscape { c: char, loc: Location },
}

impl CompilerError for LexerError {
    fn into_diagnostic(&self, _interner: &Interner) -> Diagnostic {
        use crate::common::error::diagnostic_utils::*;

        match self {
            LexerError::InvalidIntegerLiteral { loc, error } => {
                let location = Location::from_position(
                    loc.file_id(),
                    loc.span().start() + codespan::ByteOffset::from(error.index as i64),
                );

                Diagnostic::error()
                    .with_message(format!("invalid integer literal: {}", error))
                    .with_labels(vec![make_primary(location)])
            }
            LexerError::InvalidToken { loc } => Diagnostic::error()
                .with_message("invald token")
                .with_labels(vec![make_primary(*loc)]),
            LexerError::StringInvalidEscape { c, loc } => Diagnostic::error()
                .with_message(format!("unknown character escape `{}`", c))
                .with_labels(vec![make_primary(*loc)]),
        }
    }
}
