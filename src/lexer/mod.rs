//! The lexer (and related machinery)

use crate::common::{self, Locatable, Location, Span};
use codespan::ByteOffset;
use logos::Logos;

mod raw_token;
use raw_token::RawToken;

mod error;
pub use error::LexerError;

/// Semantic tokens produced after the second stage of the lexer
///
/// These tokens do not corrospond directly to the source file, but contain the
/// same meaning.
#[derive(Debug, Clone, Copy)]
pub enum Token {
    Newline,
    Ident(common::InternerID),
    Const,
    Db,
    Colon,
    Comma,
    Plus,
    Equal,
    OpenParen,
    CloseParen,
    OpenBracket,
    CloseBracket,
    IntegerLiteral(u16),
    StringLiteral(common::InternerID),
    ConditionFlag { n: bool, c: bool, z: bool },
    StackRegister,
    ARegister,
    BRegister,
    IndexRegister,
    StackPointerRegister,
    Mov,
    Sub,
    Sbb,
    Cmp,
    Add,
    Adc,
    Sbw,
    Swb,
    Nnd,
    And,
    Aib,
    Anb,
    Bia,
    Bna,
    Ora,
    Nor,
    Jmp,
    Jsr,
    Dec,
    Inc,
    Xor,
    Xnr,
    Hlt,
    Ret,
    Clc,
    Clz,
    Sec,
    Sez,
}

/// Result type used by lexer functions
pub type Result<T> = std::result::Result<T, LexerError>;

/// The lexer iterator struct
///
/// This returns a stream of tokens with locations for consumption by the parser.
pub struct Lexer<'i> {
    logos: logos::Lexer<'i, RawToken>,
    interner: &'i mut common::Interner,
    number_format: lexical_core::NumberFormat,
}

impl<'i> Lexer<'i> {
    /// Create a new lexer
    ///
    /// This function takes a reference to the interner for interning
    /// identifiers and string literals.
    pub fn new(program: &'i str, interner: &'i mut common::Interner) -> Lexer<'i> {
        let number_format = lexical_core::NumberFormat::compile(
            b'_', true, false, false, false, false, true, false, false, false, true, false, false,
            false, true, false, false, true, false, false, true, false, false, true, false, false,
            false,
        )
        .unwrap();
        Lexer {
            logos: RawToken::lexer(program),
            interner,
            number_format,
        }
    }

    /// Return the location of the current token
    fn location(&self) -> Location {
        let logos_span = self.logos.span();
        Location::new(codespan::Span::new(
            logos_span.start as u32,
            logos_span.end as u32,
        ))
    }

    /// Interpret the current token as an integer literal
    fn integer_literal(&self, radix: u8, offset: usize) -> Result<u16> {
        let value = lexical_core::parse_format_radix(
            self.logos.slice()[offset..].as_bytes(),
            radix,
            self.number_format,
        )
        .map_err(|error| LexerError::InvalidIntegerLiteral {
            loc: self.location(),
            error,
        })?;
        Ok(value)
    }

    /// Create an interned identifier from the current token
    fn identifier(&mut self) -> common::InternerID {
        self.interner.get_or_intern(self.logos.slice())
    }

    /// Create an interned string literal from the current token
    fn string_literal(&mut self) -> Result<common::InternerID> {
        let source = self.logos.slice();
        let contents = &source[1..source.len() - 1];

        let mut chars = contents.chars().enumerate();

        let mut result = String::new();

        while let Some((i, c)) = chars.next() {
            match c {
                '\\' => {
                    if let Some((_j, escape_char)) = chars.next() {
                        result.push(match escape_char {
                            '0' => '\0',
                            'n' => '\n',
                            'r' => '\r',
                            't' => '\t',
                            '\\' => '\\',
                            '"' => '"',
                            _ => {
                                let start = self.location().span().start()
                                    + ByteOffset::from_char_len('"')
                                    + ByteOffset::from(i as i64);
                                let end = start
                                    + ByteOffset::from_char_len('\\')
                                    + ByteOffset::from_char_len(escape_char);

                                let loc = Location::new(Span::new(start, end));

                                return Err(LexerError::StringInvalidEscape {
                                    c: escape_char,
                                    loc,
                                });
                            }
                        })
                    } else {
                        // This is unreachable because the string wouldn't
                        // tokenize unless the escape has some character after
                        // it.
                        unreachable!()
                    }
                }
                '"' => {
                    // This is unreachable because the string wouldn't tokenize
                    // with a quote inside.
                    unreachable!()
                }
                _ => {
                    result.push(c);
                }
            }
        }

        Ok(self.interner.get_or_intern(result))
    }

    /// Parse the current token as condition flags
    fn condition_flag(&self) -> Token {
        let mut inverted = false;
        let mut carry = false;
        let mut zero = false;

        for c in self.logos.slice()[1..].chars() {
            match c {
                'n' | 'N' => inverted = true,
                'c' | 'C' => carry = true,
                'z' | 'Z' => zero = true,
                _ => unreachable!(),
            }
        }
        Token::ConditionFlag {
            n: inverted,
            c: carry,
            z: zero,
        }
    }

    /// Get the next token from the lexer
    fn token(&mut self) -> Result<Option<Locatable<Token>>> {
        if let Some(token) = self.logos.next() {
            let token = match token {
                RawToken::Ident => Token::Ident(self.identifier()),
                RawToken::BinaryLiteral => Token::IntegerLiteral(self.integer_literal(2, 2)?),
                RawToken::OctalLiteral => Token::IntegerLiteral(self.integer_literal(8, 2)?),
                RawToken::DecimalLiteral => Token::IntegerLiteral(self.integer_literal(10, 0)?),
                RawToken::HexLiteral => Token::IntegerLiteral(self.integer_literal(16, 2)?),
                RawToken::StringLiteral => Token::StringLiteral(self.string_literal()?),
                RawToken::ConditionFlag => self.condition_flag(),

                RawToken::Error => {
                    return Err(LexerError::InvalidToken {
                        loc: self.location(),
                    })
                }

                RawToken::Newline => Token::Newline,
                RawToken::Const => Token::Const,
                RawToken::Db => Token::Db,
                RawToken::Colon => Token::Colon,
                RawToken::Comma => Token::Comma,
                RawToken::Plus => Token::Plus,
                RawToken::Equal => Token::Equal,
                RawToken::OpenParen => Token::OpenParen,
                RawToken::CloseParen => Token::CloseParen,
                RawToken::OpenBracket => Token::OpenBracket,
                RawToken::CloseBracket => Token::CloseBracket,

                RawToken::StackRegister => Token::StackRegister,
                RawToken::ARegister => Token::ARegister,
                RawToken::BRegister => Token::BRegister,
                RawToken::IndexRegister => Token::IndexRegister,
                RawToken::StackPointerRegister => Token::StackPointerRegister,

                RawToken::Mov => Token::Mov,
                RawToken::Sub => Token::Sub,
                RawToken::Sbb => Token::Sbb,
                RawToken::Cmp => Token::Cmp,
                RawToken::Add => Token::Add,
                RawToken::Adc => Token::Adc,
                RawToken::Sbw => Token::Sbw,
                RawToken::Swb => Token::Swb,
                RawToken::Nnd => Token::Nnd,
                RawToken::And => Token::And,
                RawToken::Aib => Token::Aib,
                RawToken::Anb => Token::Anb,
                RawToken::Bia => Token::Bia,
                RawToken::Bna => Token::Bna,
                RawToken::Ora => Token::Ora,
                RawToken::Nor => Token::Nor,
                RawToken::Jmp => Token::Jmp,
                RawToken::Jsr => Token::Jsr,
                RawToken::Dec => Token::Dec,
                RawToken::Inc => Token::Inc,
                RawToken::Xor => Token::Xor,
                RawToken::Xnr => Token::Xnr,
                RawToken::Hlt => Token::Hlt,
                RawToken::Ret => Token::Ret,
                RawToken::Clc => Token::Clc,
                RawToken::Clz => Token::Clz,
                RawToken::Sec => Token::Sec,
                RawToken::Sez => Token::Sez,
            };

            Ok(Some(self.location().with(token)))
        } else {
            Ok(None)
        }
    }
}

use codespan::ByteIndex;
/// Type consumed by the `lalrpop` parser
type Spanned<L, T> = Result<(L, T, L)>;

impl Iterator for Lexer<'_> {
    type Item = Spanned<ByteIndex, Token>;
    fn next(&mut self) -> Option<Spanned<ByteIndex, Token>> {
        match self.token() {
            Ok(None) => None,
            Ok(Some(token)) => Some(Ok(token.to_spanned())),
            Err(e) => Some(Err(e)),
        }
    }
}