//! Encode [`CodegenItem`]s to codegen units.
//!
//! The primary function is [`Encoder::encode_item`]

use super::{CodegenItem, Output, Unit, Word};
use crate::common::Interner;
use crate::parser::ast;
use smallvec::smallvec;

/// Instructoin encoder helper struct
pub(crate) struct Encoder<'i> {
    interner: &'i Interner,
}

/// Encode an Expression to a unit
pub(crate) fn encode_expr(expr: &ast::Expr) -> Unit {
    match expr {
        ast::Expr::Literal(val) => Unit::Value(*val),
        ast::Expr::Variable(id) => Unit::Variable(*id),
    }
}

/// Encode a direct register for use in operands or reference `mov`
fn encode_direct_register(reg: &ast::DirectRegister) -> Word {
    match reg {
        ast::DirectRegister::A => 0b00,
        ast::DirectRegister::B => 0b01,
        ast::DirectRegister::Index => 0b10,
        ast::DirectRegister::StackPointer => 0b11,
    }
}

/// Encode an operand
fn encode_operand(op: &ast::Operand) -> Word {
    match op {
        ast::Operand::DirectRegister(reg) => encode_direct_register(reg),
        ast::Operand::Immediate(_) => 0b100,
        ast::Operand::ImmediateReference(ast::Reference::Reference(_)) => 0b101,
        ast::Operand::ImmediateReference(ast::Reference::ReferenceIndexed(_)) => 0b110,
        ast::Operand::Stack => 0b111,
    }
}

/// Encode the condition flags
fn encode_condition(cond: &ast::Condition) -> Word {
    (if cond.invert { 1 } else { 0 } << 2)
        + (if cond.zero { 1 } else { 0 } << 1)
        + (if cond.carry { 1 } else { 0 })
}

impl Encoder<'_> {
    /// Create a new instruction encoder
    ///
    /// The interner is used for resolving variables during encoding for resolving variables
    pub(crate) fn new(interner: &Interner) -> Encoder {
        Encoder { interner }
    }

    // TODO: Move a bunch of these functions out

    /// Encode an implied operation (no operands)
    fn encode_implied_operation(
        &self,
        op: &ast::ImpliedOperation,
        cond: &ast::Condition,
    ) -> Output {
        let condition_value = encode_condition(cond);
        let instr = match op.instruction {
            ast::ImpliedInstruction::Clc => 0b111_100,
            ast::ImpliedInstruction::Clz => 0b111_101,
            ast::ImpliedInstruction::Hlt => 0b110_001,
            ast::ImpliedInstruction::Ret => 0b110_011,
            ast::ImpliedInstruction::Sec => 0b111_110,
            ast::ImpliedInstruction::Sez => 0b111_111,
        };
        smallvec![Unit::Value(instr << 3 | condition_value << 9)]
    }

    /// Encode an instruction and operand
    fn encode_instruction_operand(
        &self,
        instr: Word,
        operand: &ast::Operand,
        cond: &ast::Condition,
    ) -> Output {
        let condition_value = encode_condition(cond) << 9;
        let operand_value = encode_operand(operand);

        match operand {
            ast::Operand::Immediate(e)
            | ast::Operand::ImmediateReference(ast::Reference::Reference(e))
            | ast::Operand::ImmediateReference(ast::Reference::ReferenceIndexed(e)) => {
                smallvec![
                    Unit::Value(instr | operand_value | condition_value),
                    encode_expr(e),
                ]
            }
            _ => smallvec![Unit::Value(instr | operand_value | condition_value)],
        }
    }

    /// Encode a unary operation (one operand)
    fn encode_unary_operation(&self, op: &ast::UnaryOperation, cond: &ast::Condition) -> Output {
        let instr = match op.instruction {
            ast::UnaryInstruction::Add => 0b100_000,
            ast::UnaryInstruction::Adc => 0b100_001,
            ast::UnaryInstruction::Sbw => 0b100_110,
            ast::UnaryInstruction::Swb => 0b100_111,
            ast::UnaryInstruction::Nnd => 0b101_000,
            ast::UnaryInstruction::And => 0b101_001,
            ast::UnaryInstruction::Aib => 0b101_010,
            ast::UnaryInstruction::Anb => 0b101_011,
            ast::UnaryInstruction::Bia => 0b101_100,
            ast::UnaryInstruction::Bna => 0b101_101,
            ast::UnaryInstruction::Ora => 0b101_110,
            ast::UnaryInstruction::Nor => 0b101_111,
            ast::UnaryInstruction::Jmp => 0b110_000,
            ast::UnaryInstruction::Jsr => 0b110_010,
            ast::UnaryInstruction::Dec => 0b110_100,
            ast::UnaryInstruction::Inc => 0b110_101,
            ast::UnaryInstruction::Xor => 0b111_010,
            ast::UnaryInstruction::Xnr => 0b111_011,
        };

        self.encode_instruction_operand(instr << 3, &op.operand, cond)
    }

    /// Encode a binary operation (one operand + A register)
    fn encode_binary_operation(&self, op: &ast::BinaryOperation, cond: &ast::Condition) -> Output {
        let instr = match (op.instruction, op.flipped) {
            (ast::BinaryInstruction::Sub, false) => 0b100_010,
            (ast::BinaryInstruction::Sub, true) => 0b100_100,
            (ast::BinaryInstruction::Sbb, false) => 0b100_011,
            (ast::BinaryInstruction::Sbb, true) => 0b100_101,
            (ast::BinaryInstruction::Cmp, false) => 0b111_000,
            (ast::BinaryInstruction::Cmp, true) => 0b111_001,
        };

        self.encode_instruction_operand(instr << 3, &op.operand, cond)
    }

    /// Encode a `mov` operation
    fn encode_mov_operation(&self, op: &ast::MovOperation, cond: &ast::Condition) -> Output {
        match op {
            ast::MovOperation::Mov { op1, op2 } => {
                let operand = encode_operand(op2);

                let mut output = self.encode_instruction_operand(operand << 3, op1, cond);

                match op2 {
                    ast::Operand::Immediate(e)
                    | ast::Operand::ImmediateReference(ast::Reference::Reference(e))
                    | ast::Operand::ImmediateReference(ast::Reference::ReferenceIndexed(e)) => {
                        output.push(encode_expr(e));
                    }
                    _ => {}
                };

                output
            }
            ast::MovOperation::MovReference { op, reg, flipped } => {
                let instr = match flipped {
                    false => 0b100,
                    true => 0b010,
                };

                let (op, r_bit) = match op {
                    ast::Reference::Reference(o) => (o, 0),
                    ast::Reference::ReferenceIndexed(o) => (o, 1),
                };

                let reg_val = encode_direct_register(reg);

                self.encode_instruction_operand(instr << 6 | r_bit << 5 | reg_val << 3, op, cond)
            }
        }
    }

    /// Encode any operation
    fn encode_operation(&self, op: &ast::Operation, cond: &ast::Condition) -> Output {
        match op {
            ast::Operation::Implied(imp_op) => self.encode_implied_operation(imp_op, cond),
            ast::Operation::Unary(unary_op) => self.encode_unary_operation(unary_op, cond),
            ast::Operation::Binary(binary_op) => self.encode_binary_operation(binary_op, cond),
            ast::Operation::Mov(mov_op) => self.encode_mov_operation(mov_op, cond),
        }
    }

    /// Encode any operation and the condition
    fn encode_condition_operation(&self, cond_op: &ast::ConditionOperation) -> Output {
        self.encode_operation(&cond_op.op, &cond_op.cond)
    }

    /// Encode a single item from a `db` directive
    fn encode_data(&self, data: &ast::DbData) -> Output {
        match data {
            ast::DbData::Expr(e) => smallvec![encode_expr(e)],
            ast::DbData::Data(s) => self
                .interner
                .resolve(s)
                .as_bytes()
                .iter()
                .map(|b| Unit::Value(*b as u16))
                .collect(),
        }
    }

    /// Encode a [`CodegenItem`]
    pub(crate) fn encode_item(&self, item: &CodegenItem) -> Output {
        match item {
            CodegenItem::Constant(_, _) => smallvec![],
            CodegenItem::Data(data) => self.encode_data(data),
            CodegenItem::Operation(op) => self.encode_condition_operation(op),
        }
    }
}
