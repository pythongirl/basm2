//! Code generation and instruction encoding
//!
//! The main function to be used is [`Generator::generate`]

use crate::common::{Locatable, Interner, InternerID};
use crate::parser::ast;
use smallvec::SmallVec;
use std::collections::HashMap;

/// Type used to represent the 12-bit machine words imperfectly.
pub type Word = u16;

mod error;
pub use error::{SemanticError, Result};

mod encode;

/// Unit used during codegen
///
/// Each unit will represent one word in the final output.
#[derive(Debug, Clone, Copy)]
pub enum Unit {
    Value(Word),
    Variable(InternerID),
    CurrentPosition,
}

/// Convenience output type used while encoding instructions.
///
/// Using this type will *not* cause a heap allocation for individual operations
/// which will at most take up 3 units. This will only cause heap allocations
/// for `db` directives that output more than 4 machine words.
pub type Output = SmallVec<[Unit; 4]>;

/// Codegen items corrosponding to [`ast::Item`]s
pub(crate) enum CodegenItem {
    /// Constant definition or label in source code
    Constant(InternerID, Unit),
    /// Opertion code
    Operation(ast::ConditionOperation),
    /// Single item of a `db` directive
    Data(ast::DbData),
}

/// Translate an iterator of [`ast::Item`]s into a vector of [`CodegenItem`]s
fn translate_items(ast_items: impl IntoIterator<Item = Locatable<ast::Item>>) -> Vec<Locatable<CodegenItem>> {
    let mut items = Vec::new();

    for item in ast_items {
        match item.data {
            ast::Item::Label(name) => {
                items.push(item.location.with(CodegenItem::Constant(name, Unit::CurrentPosition)));
            }
            ast::Item::ConstantDefinition { name, value } => {
                items.push(item.location.with(CodegenItem::Constant(name, encode::encode_expr(&value))));
            }
            ast::Item::DbDirective(data) => {
                for datum in data {
                    items.push(datum.location.with(CodegenItem::Data(datum.data)));
                }
            }
            ast::Item::Operation(op) => items.push(item.location.with(CodegenItem::Operation(op))),
        }
    }

    items
}

/// Struct used for keeping track of codegen's internal state
pub(crate) struct Generator {
    offset: Word,
    variables: HashMap<InternerID, Locatable<Word>>,
}

impl Generator {
    /// Create a new codegen state
    fn new() -> Generator {
        Generator {
            offset: 0,
            variables: HashMap::new(),
        }
    }

    /// Evalute a single unit
    ///
    /// This function will evaluate a single unit and return a semantic error if
    /// the unit refers to an undefined variable.
    fn evaluate_unit(&self, unit: Locatable<Unit>) -> Result<Word> {
        Ok(match unit.data {
            Unit::CurrentPosition => self.offset,
            Unit::Value(val) => val,
            Unit::Variable(name) => {
                if let Some(val) = self.variables.get(&name) {
                    val.data
                } else {
                    return Err(SemanticError::UndefinedVariable {
                        name,
                        loc: unit.location,
                    });
                }
            }
        })
    }

    /// Declare a constant in the variable map
    ///
    /// Declares a varible in the constant map as a specific value with a
    /// location. This function will return an error if the variable already
    /// exists.
    fn declare_constant(&mut self, name: InternerID, term: Locatable<Unit>) -> Result<()> {
        let value = self.evaluate_unit(term)?;

        let other_decl = self.variables.insert(name, term.location.with(value));

        if let Some(other_decl) = other_decl {
            return Err(SemanticError::DoubleDelcaration {
                name,
                loc1: other_decl.location,
                loc2: term.location,
            });
        }

        Ok(())
    }

    /// Generate code from a series of [`ast::Item`]s into machine code.
    ///
    /// The interner must be passed in so that string literals can be resolved.
    pub(crate) fn generate(program: Vec<Locatable<ast::Item>>, interner: &Interner) -> Result<Vec<Word>> {
        // TODO: Allow for multiple SemanticErrors to be detected and returned

        let encoder = encode::Encoder::new(interner);

        let items = translate_items(program);
        let results = items
            .iter()
            .map(|i| encoder.encode_item(&i.data))
            .collect::<Vec<_>>();

        let mut output = Vec::new();

        let mut state = Generator::new();

        for (item, result) in items.iter().zip(&results) {
            state.offset += result.len() as u16;

            if let CodegenItem::Constant(name, value) = item.data {
                state.declare_constant(name, item.location.with(value))?;
            }

            for &unit in result {
                output.push(state.evaluate_unit(item.location.with(unit))?);
            }
        }

        Ok(output)
    }
}
