//! AST (Abstract Syntax Tree) to represent basm assembly code
//!
//! The parser produces [`Item`]s from parsing a program.

pub use crate::common::{Locatable, InternerID};

/// Top-level items produced by the parser
#[derive(Debug, Clone)]
pub enum Item {
    /// Label representing a specific location in memory
    Label(InternerID),
    /// Definition for a constant
    ConstantDefinition { name: InternerID, value: Expr },
    /// `db` directive for including raw data in the program
    DbDirective(Vec<Locatable<DbData>>),
    /// Operation that will be translated to code
    Operation(ConditionOperation),
}

/// One item of data to be outputed by the [`db` directive](`Item::DbDirective`).
#[derive(Debug, Clone)]
pub enum DbData {
    Data(InternerID),
    Expr(Expr),
}

/// An expression which can be used in many contexts
///
/// Expressions can be used for the values in constant definitions, as items in
/// a [`db` directive](`Item::DbDirective`), or as an immediate value in an
/// operation.
#[derive(Debug, Clone, PartialEq)]
pub enum Expr {
    Variable(InternerID),
    Literal(u16),
}

/// Operand used as an argument to an [`Operation`]
#[derive(Debug, Clone, PartialEq)]
pub enum Operand {
    DirectRegister(DirectRegister),
    Immediate(Expr),
    ImmediateReference(Reference<Expr>),
    Stack,
}

/// Generic reference type used in [`Operand`]s and in
/// [reference `mov`s](`MovOperation::MovReference`)
#[derive(Debug, Clone, PartialEq)]
pub enum Reference<T> {
    Reference(T),
    ReferenceIndexed(T),
}

/// A direct register argument
///
/// Can be any of the A register, the B register, the Index register, or the
/// Stack Pointer regsiter.
#[derive(Debug, Clone, PartialEq)]
pub enum DirectRegister {
    A,
    B,
    Index,
    StackPointer,
}

/// All instructions available in an implied operation
#[derive(Debug, Clone, Copy)]
pub enum ImpliedInstruction {
    Hlt,
    Ret,
    Clc,
    Clz,
    Sec,
    Sez,
}

/// Implied operation (no operands)
#[derive(Debug, Clone)]
pub struct ImpliedOperation {
    pub instruction: ImpliedInstruction,
}

/// All instructions available in unary operations
#[derive(Debug, Clone, Copy)]
pub enum UnaryInstruction {
    Add,
    Adc,
    Sbw,
    Swb,
    Nnd,
    And,
    Aib,
    Anb,
    Bia,
    Bna,
    Ora,
    Nor,
    Jmp,
    Jsr,
    Dec,
    Inc,
    Xor,
    Xnr,
}

/// Unary operations (one operand)
#[derive(Debug, Clone)]
pub struct UnaryOperation {
    pub instruction: UnaryInstruction,
    pub operand: Operand,
}

/// All instructions available for binary operations.
#[derive(Debug, Clone, Copy)]
pub enum BinaryInstruction {
    Sub,
    Sbb,
    Cmp,
}

/// Binary operation (two arguments)
///
/// Binary operations are somewhat special because at least one of the two
/// operands provided to the operation must be `Ac` or the A regiser.
#[derive(Debug, Clone)]
pub struct BinaryOperation {
    pub instruction: BinaryInstruction,
    pub operand: Operand,
    pub flipped: bool,
}

/// The `mov` operation
///
/// Even though `mov` operations take two arguments, they are separate from
/// the [binary operations](`BinaryOperation`) because they are encoded
/// very differently. `mov` operations come in two variants.
#[derive(Debug, Clone)]
pub enum MovOperation {
    /// The basic `mov` form takes any two operands
    Mov {
        op1: Operand,
        op2: Operand,
    },
    /// The reference `mov` operation requires one operand to be a reference in
    /// square brackets and the other operand must be a
    /// [direct register](`DirectRegister`).
    MovReference {
        op: Reference<Operand>,
        reg: DirectRegister,
        flipped: bool,
    },
}

/// Any operation that may be executed by the computer
#[derive(Debug, Clone)]
pub enum Operation {
    Implied(ImpliedOperation),
    Unary(UnaryOperation),
    Binary(BinaryOperation),
    Mov(MovOperation),
}

/// A set of condition flags for the computer to check before executing an
/// instruction
#[derive(Debug, Clone, Copy)]
pub struct Condition {
    pub invert: bool,
    pub zero: bool,
    pub carry: bool,
}

impl Condition {
    /// The set of condition flags representing and unconditional execution.
    pub const NONE: Condition = Condition {
        invert: false,
        zero: false,
        carry: false,
    };
}

/// An [operation](`Operation`) with [conditonal flags](`Condition`)
#[derive(Debug, Clone)]
pub struct ConditionOperation {
    pub op: Operation,
    pub cond: Condition,
}
